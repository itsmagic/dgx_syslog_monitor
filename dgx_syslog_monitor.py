import wx
import os
import sys
import json
import scripts.dgx_syslog_gui as dgx_syslog_gui
from pydispatch import dispatcher
import wmi
import time
from scripts.dgx_syslog_control import Control
from scripts.dgx_syslog_monitor import SyslogMonitor
from scripts.command_queue import Commandjobs
from scripts import auto_update
from license_base import LicenseBase
import queue
import webbrowser

# The MIT License (MIT)

# Copyright (c) 2023 Jim Maciejewski

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

class DGX_Syslog_Frame(dgx_syslog_gui.DGXSyslogFrame):
    def __init__(self, parent):
        dgx_syslog_gui.DGXSyslogFrame.__init__(self, parent)

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "dgx.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)


        self.name = "DGX_Syslog_Monitor"
        self.version = "v1.0.3"
        self.SetTitle(self.name + " " + self.version)
        self.log_file_name = "syslog_" + time.strftime("%Y%m%d-%H%M%S") + '.txt'
        self.file_storage_txt.SetLabel(self.resource_path(self.log_file_name))
        self.my_control = None
        self.bcpu_buttons = []
        self.hide_all()
        self.get_pc_info()
        self.load_config()
        dispatcher.connect(self.status,
                           signal="Status",
                           sender=dispatcher.Any)
        dispatcher.connect(self.telnet_results,
                           signal="Results",
                           sender=dispatcher.Any)
        dispatcher.connect(self.syslog,
                           signal="Syslog",
                           sender=dispatcher.Any)

        self.command_job_queue = queue.Queue()
        self.command_queue = Commandjobs(self, self.command_job_queue)
        self.command_queue.daemon = True
        self.command_queue.start()
        
        self.monitor = SyslogMonitor()
        self.monitor.daemon = True
        self.monitor.start()

        self.check_for_updates = True
        dispatcher.connect(self.update_required,
                           signal="Software Update",
                           sender=dispatcher.Any)
        if self.check_for_updates:
            update_thread = auto_update.AutoUpdate(server_url="https://magicsoftware.ornear.com", program_name=self.name, program_version=self.version)
            update_thread.daemon = True
            update_thread.start()

            self.license_thread = LicenseBase(name=self.name, require_serial_number=False)
            self.license_thread.daemon = True
            self.license_thread.start()

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)
    
    def update_required(self, sender, message, url):
        """Show the update page"""
        dlg = wx.MessageDialog(parent=self,
                               message='An update is available. \rWould you like to go to the download page?',
                               caption='Update available',
                               style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            webbrowser.open(url)

    def syslog(self, message):
        # print('message: ', message)
        msg = message['msg']
        source = (message['source'][0])
        message_txt = f"{source} : {msg}"
        # print('message: ', repr(message_txt))
        self.syslog_txt.AppendText(message_txt)
        with open(self.log_file_name, 'a', encoding='utf-8') as f:
            f.write(message_txt)

    def telnet_results(self, results):
        if 'dgx' in results:
            self.num_bcpus = results['dgx'] // 200
            for i in range(self.num_bcpus):
                bcpu = 'bcpu' + str(i + 1)
                self.command_job_queue.put({'syslog_enabled': True, 'bcpu': bcpu.upper()})
            self.Layout()
        if 'bcpu' in results:
            # Update the gui with enabled units
            checkbox = results['bcpu'].lower() + '_chk'
            # print('bcpu enable: ', results['enabled'])
            getattr(self, checkbox).Enable()
            getattr(self, checkbox).SetValue(results['enabled'])
            getattr(self, checkbox).Show()

    def status(self, status):
        if status == 'Connected':
            # print('Got connected')
            self.connect_btn.SetLabel('Disconnect')
            self.dgx_ip_address_txt.Disable()
            self.command_job_queue.put({'query': True})
        if status == 'Disconnected':
            self.my_control = None
            self.connect_btn.SetLabel('Connect')
            self.dgx_ip_address_txt.Enable()
            self.hide_all()

    def hide_all(self):
        """Hide all the checkboxes on startup"""
        for i in range(32):
            bcpu = 'bcpu' + str(i + 1) + '_chk'
            getattr(self, bcpu).Hide()
        

    def on_enable(self, event):
        # print('in event')
        bcpu = event.GetEventObject().GetLabel()
        getattr(self, bcpu.lower() + '_chk').Disable()
        ip_address = self.pc_ip_cmb.GetValue()
        mac_address = self.pc_mac_cmb.GetValue()
        self.command_job_queue.put({'bcpu': bcpu,
                                    'ip_address': ip_address,
                                    'mac_address': mac_address,
                                    'enable': event.GetEventObject().IsChecked()
                                    })

    def on_connect(self, event):
        """Connect to the DGX"""
        self.connect_btn.SetLabel('Waiting')
        if self.my_control is not None:
            if self.my_control.connected:
                # we are connected time to disconnect
                # self.command_job_queue.join()
                self.my_control.disconnect()
                return
        else:
            ip_address = self.dgx_ip_address_txt.GetValue()
            self.my_control = Control(ip_address)
        self.my_control.connect()

    def get_pc_info(self):
        """Attempts to get the IP address and MAC address of the host PC"""
        configs = wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True)
        # print(configs)
        for i in range(len(configs)):
            if configs[i].IPEnabled:
                ip_address = configs[i].IPAddress[0]
                mac_address = configs[i].MACAddress
                self.pc_ip_cmb.Append(ip_address)
                self.pc_mac_cmb.Append(mac_address)
        self.pc_ip_cmb.SetSelection(0)
        self.pc_mac_cmb.SetSelection(0)

    def ip_selected(self, event):
        self.pc_mac_cmb.SetSelection(self.pc_ip_cmb.GetSelection())

    def mac_selected(self, event):
        self.pc_ip_cmb.SetSelection(self.pc_mac_cmb.GetSelection())

    def load_config(self):
        """Loads the saved config"""
        try:
            with open(self.name + '.conf', 'r') as f:
                config = json.load(f)
            if "dgx_ip_address" in config:
                self.dgx_ip_address_txt.SetValue(config['dgx_ip_address'])
        except Exception as error:
            print("unable to load configuration: ", repr(error))

    def save_config(self, event=None):
        """Save the ip addresses for next time"""
        try:
            dgx_ip_address = self.dgx_ip_address_txt.GetValue()
        except AttributeError:
            dgx_ip_address = ''
        # try:
        with open(self.name + '.conf', 'w') as f:
            config = {"dgx_ip_address": dgx_ip_address}
            json.dump(config, f)
        # except Exception as error:
        #     print("unable to save configuration: ", repr(error))


def main():
    """run the main program"""
    ds_app = wx.App() # redirect=True, filename="log.txt")
    # do processing/initialization here and create main window
    ds_frame = DGX_Syslog_Frame(None)
    ds_frame.Show()
    ds_app.MainLoop()


if __name__ == '__main__':
    main()
