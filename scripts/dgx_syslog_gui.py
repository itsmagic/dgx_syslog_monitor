# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class DGXSyslogFrame
###########################################################################

class DGXSyslogFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 1087,600 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		bSizer2 = wx.BoxSizer( wx.HORIZONTAL )

		sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Destination Information" ), wx.VERTICAL )

		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText1 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"DGX Shell IP Address", wx.DefaultPosition, wx.Size( 110,-1 ), 0 )
		self.m_staticText1.Wrap( -1 )

		bSizer4.Add( self.m_staticText1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.dgx_ip_address_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 120,-1 ), 0 )
		bSizer4.Add( self.dgx_ip_address_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.connect_btn = wx.Button( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Connect", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.connect_btn, 0, wx.ALL, 5 )


		sbSizer1.Add( bSizer4, 1, wx.EXPAND, 5 )

		bSizer5 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText2 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"PC IP Address", wx.DefaultPosition, wx.Size( 110,-1 ), 0 )
		self.m_staticText2.SetLabelMarkup( u"PC IP Address" )
		self.m_staticText2.Wrap( -1 )

		bSizer5.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		pc_ip_cmbChoices = []
		self.pc_ip_cmb = wx.ComboBox( sbSizer1.GetStaticBox(), wx.ID_ANY, u"None", wx.DefaultPosition, wx.Size( 120,-1 ), pc_ip_cmbChoices, 0 )
		bSizer5.Add( self.pc_ip_cmb, 0, wx.ALL, 5 )


		sbSizer1.Add( bSizer5, 1, wx.EXPAND, 5 )

		bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText3 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"PC MAC Address", wx.DefaultPosition, wx.Size( 110,-1 ), 0 )
		self.m_staticText3.Wrap( -1 )

		bSizer6.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		pc_mac_cmbChoices = []
		self.pc_mac_cmb = wx.ComboBox( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 120,-1 ), pc_mac_cmbChoices, 0 )
		bSizer6.Add( self.pc_mac_cmb, 0, wx.ALL, 5 )


		sbSizer1.Add( bSizer6, 1, wx.EXPAND, 5 )


		bSizer2.Add( sbSizer1, 0, wx.ALL, 5 )

		self.button_sizer = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		bSizer13 = wx.BoxSizer( wx.VERTICAL )

		bSizer14 = wx.BoxSizer( wx.HORIZONTAL )

		self.bcpu1_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU1", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu1_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer14.Add( self.bcpu1_chk, 0, wx.ALL, 5 )

		self.bcpu2_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU2", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu2_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer14.Add( self.bcpu2_chk, 0, wx.ALL, 5 )

		self.bcpu3_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU3", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu3_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer14.Add( self.bcpu3_chk, 0, wx.ALL, 5 )

		self.bcpu4_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU4", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu4_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer14.Add( self.bcpu4_chk, 0, wx.ALL, 5 )

		self.bcpu5_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU5", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu5_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer14.Add( self.bcpu5_chk, 0, wx.ALL, 5 )

		self.bcpu6_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU6", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu6_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer14.Add( self.bcpu6_chk, 0, wx.ALL, 5 )

		self.bcpu7_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU7", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu7_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer14.Add( self.bcpu7_chk, 0, wx.ALL, 5 )

		self.bcpu8_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU8", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu8_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer14.Add( self.bcpu8_chk, 0, wx.ALL, 5 )


		bSizer13.Add( bSizer14, 1, wx.EXPAND, 5 )

		bSizer15 = wx.BoxSizer( wx.HORIZONTAL )

		self.bcpu9_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU9", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu9_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer15.Add( self.bcpu9_chk, 0, wx.ALL, 5 )

		self.bcpu10_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU10", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu10_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer15.Add( self.bcpu10_chk, 0, wx.ALL, 5 )

		self.bcpu11_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU11", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu11_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer15.Add( self.bcpu11_chk, 0, wx.ALL, 5 )

		self.bcpu12_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU12", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu12_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer15.Add( self.bcpu12_chk, 0, wx.ALL, 5 )

		self.bcpu13_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU13", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu13_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer15.Add( self.bcpu13_chk, 0, wx.ALL, 5 )

		self.bcpu14_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU14", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu14_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer15.Add( self.bcpu14_chk, 0, wx.ALL, 5 )

		self.bcpu15_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU15", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu15_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer15.Add( self.bcpu15_chk, 0, wx.ALL, 5 )

		self.bcpu16_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU16", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu16_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer15.Add( self.bcpu16_chk, 0, wx.ALL, 5 )


		bSizer13.Add( bSizer15, 1, wx.EXPAND, 5 )

		bSizer16 = wx.BoxSizer( wx.HORIZONTAL )

		self.bcpu17_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU17", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu17_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer16.Add( self.bcpu17_chk, 0, wx.ALL, 5 )

		self.bcpu18_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU18", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu18_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer16.Add( self.bcpu18_chk, 0, wx.ALL, 5 )

		self.bcpu19_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU19", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu19_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer16.Add( self.bcpu19_chk, 0, wx.ALL, 5 )

		self.bcpu20_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU20", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu20_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer16.Add( self.bcpu20_chk, 0, wx.ALL, 5 )

		self.bcpu21_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU21", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu21_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer16.Add( self.bcpu21_chk, 0, wx.ALL, 5 )

		self.bcpu22_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU22", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu22_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer16.Add( self.bcpu22_chk, 0, wx.ALL, 5 )

		self.bcpu23_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU23", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu23_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer16.Add( self.bcpu23_chk, 0, wx.ALL, 5 )

		self.bcpu24_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU24", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu24_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer16.Add( self.bcpu24_chk, 0, wx.ALL, 5 )


		bSizer13.Add( bSizer16, 1, wx.EXPAND, 5 )

		bSizer17 = wx.BoxSizer( wx.HORIZONTAL )

		self.bcpu25_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU25", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu25_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer17.Add( self.bcpu25_chk, 0, wx.ALL, 5 )

		self.bcpu26_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU26", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu26_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer17.Add( self.bcpu26_chk, 0, wx.ALL, 5 )

		self.bcpu27_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU27", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu27_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer17.Add( self.bcpu27_chk, 0, wx.ALL, 5 )

		self.bcpu28_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU28", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu28_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer17.Add( self.bcpu28_chk, 0, wx.ALL, 5 )

		self.bcpu29_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU29", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu29_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer17.Add( self.bcpu29_chk, 0, wx.ALL, 5 )

		self.bcpu30_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU30", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu30_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer17.Add( self.bcpu30_chk, 0, wx.ALL, 5 )

		self.bcpu31_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU31", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu31_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer17.Add( self.bcpu31_chk, 0, wx.ALL, 5 )

		self.bcpu32_chk = wx.CheckBox( self.button_sizer.GetStaticBox(), wx.ID_ANY, u"BCPU32", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.bcpu32_chk.SetMinSize( wx.Size( 60,-1 ) )

		bSizer17.Add( self.bcpu32_chk, 0, wx.ALL, 5 )


		bSizer13.Add( bSizer17, 1, wx.EXPAND, 5 )


		self.button_sizer.Add( bSizer13, 1, wx.EXPAND, 5 )

		bSizer12 = wx.BoxSizer( wx.VERTICAL )


		self.button_sizer.Add( bSizer12, 0, wx.ALIGN_RIGHT, 5 )


		bSizer2.Add( self.button_sizer, 1, wx.EXPAND|wx.ALL, 5 )


		bSizer1.Add( bSizer2, 0, wx.EXPAND, 5 )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		bSizer13 = wx.BoxSizer( wx.VERTICAL )

		self.syslog_txt = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
		bSizer13.Add( self.syslog_txt, 1, wx.EXPAND, 5 )


		bSizer3.Add( bSizer13, 1, wx.EXPAND, 5 )

		bSizer14 = wx.BoxSizer( wx.HORIZONTAL )

		self.file_storage_txt = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.file_storage_txt.Wrap( -1 )

		bSizer14.Add( self.file_storage_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer3.Add( bSizer14, 0, wx.EXPAND, 5 )


		bSizer1.Add( bSizer3, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.dgx_ip_address_txt.Bind( wx.EVT_TEXT, self.save_config )
		self.connect_btn.Bind( wx.EVT_BUTTON, self.on_connect )
		self.pc_ip_cmb.Bind( wx.EVT_COMBOBOX, self.ip_selected )
		self.pc_mac_cmb.Bind( wx.EVT_COMBOBOX, self.mac_selected )
		self.bcpu1_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu2_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu3_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu4_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu5_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu6_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu7_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu8_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu9_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu10_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu11_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu12_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu13_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu14_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu15_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu16_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu17_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu18_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu19_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu20_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu21_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu22_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu23_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu24_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu25_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu26_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu27_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu28_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu29_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu30_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu31_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )
		self.bcpu32_chk.Bind( wx.EVT_CHECKBOX, self.on_enable )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def save_config( self, event ):
		event.Skip()

	def on_connect( self, event ):
		event.Skip()

	def ip_selected( self, event ):
		event.Skip()

	def mac_selected( self, event ):
		event.Skip()

	def on_enable( self, event ):
		event.Skip()

































