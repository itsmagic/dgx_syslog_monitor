import socket
from pydispatch import dispatcher
from threading import Thread
import time
import logging

logger = logging.getLogger("SyslogMonitor")

class SyslogMonitor(Thread):

    def __init__(self):
        self.shutdown = False
        Thread.__init__(self)

    def run(self):
        dispatcher.send(signal="Syslog", message={"msg": 'Starting listener\n', "source": ('*** syslog-thread', 0)})
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            server_address = ('', 514)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind(server_address)
            sock.setblocking(0)

        except IOError:
            # something has already used the port, its probably not us because
            # multiple instances can run without error due to SO_REUSEADDR
            dispatcher.send(signal="Status", status="Unable to start UDP listener")
        my_msg = 'Listening on %s port %s' % server_address
        dispatcher.send(signal="Syslog", message={"msg": my_msg + '\n', "source": ('*** syslog-thread', 0)})
        while not self.shutdown:
            try:
                msg, source = sock.recvfrom(1024)
            except Exception as error:
                if error.args[0] == 10035:
                    # no data
                    # lets wait a bit and check again
                    time.sleep(1)
                    continue
                else:
                    logging.error(f"Error listening: {error}")
            # try:
            my_msg = msg.decode('utf-8', 'ignore')
            dispatcher.send(signal="Syslog", message={"msg": my_msg, "source": source})
            # except Exception as error:
            #     print('Error in listen: ', error)


def syslog(message):
    print(message)


def main():
    dispatcher.connect(syslog,
                       signal="Syslog",
                       sender=dispatcher.Any)
    test = SyslogMonitor()
    test.setDaemon(True)
    test.start()
    time.sleep(10)


if __name__ == "__main__":
    main()
