import telnetlib
from pydispatch import dispatcher
import time
import logging

logger = logging.getLogger("Control")



class Control:

    def __init__(self, ip_address, port=23):
        self.shutdown = False
        self.connected = False
        self.ip_address = ip_address
        self.port = port
        # self.connect()
        dispatcher.connect(self.command,
                           signal="Telnet Command",
                           sender=dispatcher.Any)

    def connect(self):
        if not self.shutdown:
            try:
                self.ts = self.establish_telnet(self.ip_address, self.port)
            except Exception as error:
                # If we fail here we probably couldn't connect to the server
                dispatcher.send(signal="Syslog", message={"msg": 'Error connecting: ' + repr(error) + '\n', "source": ('*** control-thread', 0)})
                dispatcher.send(signal="Status", status="Disconnected")
                return
            # print('read until')
            # print(self.ts.read_eager())
            self.ts.write(b'\n')
            result = self.ts.read_until(b'DGX_SHELL>', 10)
            if 'DGX_SHELL>' in result.decode():
                self.connected = True
                dispatcher.send(signal="Status", status="Connected")
            else:
                dispatcher.send(signal="Status", status="Unable to Connect")

    def command(self, sender, command):
        # print('Got command: ', command, sender)
        # Commands should include bcpu
        try:
            if 'query' in command and self.connected:
                self.ts.write(b'splash\n')
                my_result = self.ts.read_until(b'DGX_SHELL>', 10)
                # print(my_result.decode())
                dgx_type = int(my_result.decode().split()[5])
                dispatcher.send(signal='Results', results={'dgx': dgx_type})

            if 'syslog_enabled' in command and self.connected:
                my_cmd = 'set ' + command['bcpu'] + '_syslog_enabled\n'
                self.ts.write(my_cmd.encode())
                my_result = self.ts.read_until(b'DGX_SHELL>', 10)
                # print('my_result: ', my_result)
                try:
                    # Check if it is on or off
                    enabled = command['bcpu'] + '_syslog_enabled=ON' in my_result.decode()
                    dispatcher.send(signal='Results', results={'bcpu': command['bcpu'].lower(), 'enabled': enabled})
                except Exception as error:
                    logger.error(f'Unable to check syslog: {error}')

            if 'ip_address' in command:
                # this means to set the BCPU listed to this IP address\
                my_command = 'set ' + command['bcpu'] + '_syslog_server_ip_address ' + command['ip_address'] + '\n'
                my_result = command['bcpu'] + '_syslog_server_ip_address=' + command['ip_address']
                self.ts.write(my_command.encode())
                result = self.ts.read_until(b'DGX_SHELL>', 10)
                if my_result in result.decode():
                    # print('success', result.decode())
                    pass
                else:
                    # print('fail ', my_result)
                    logger.warning(f'command didnt match:  {result.decode()}')
            if 'mac_address' in command:
                my_command = 'set ' + command['bcpu'] + '_syslog_server_mac_address ' + command['mac_address'] + '\n'
                my_result = command['bcpu'] + '_syslog_server_mac_address=' + command['mac_address'].lower()
                self.ts.write(my_command.encode())
                result = self.ts.read_until(b'DGX_SHELL>', 10)
                if my_result in result.decode():
                    # print('success', result.decode())
                    pass
                else:
                    # print('fail ', my_result)
                    logger.warning(f'command didnt match {result.decode()}')
            if 'enable' in command:
                # enable syslog for a BCPU
                if command['enable']:
                    enable = 'ON'
                else:
                    enable = 'OFF'
                my_command = 'set ' + command['bcpu'] + '_syslog_enabled ' + enable + '\n'
                my_result = command['bcpu'] + '_syslog_enabled=' + enable
                logging.info(f'Sending command: {my_command}')
                self.ts.write(my_command.encode())
                result = self.ts.read_until(b'DGX_SHELL>', 10)
                logging.info(f"Checking: {command['bcpu']}_syslog_enabled={enable} is in {result.decode()}")
                success = command['bcpu'] + '_syslog_enabled=' + enable in result.decode()
                if not success:
                    dispatcher.send(signal="Syslog", message={"msg": 'Error enabling syslog on ' + command['bcpu'] + '\n', "source": ('*** control-thread', 0)})
                    dispatcher.send(signal='Results', results={'bcpu': command['bcpu'].lower(), 'enabled': not command['enable']})
                else:
                    dispatcher.send(signal='Results', results={'bcpu': command['bcpu'].lower(), 'enabled': command['enable']})
                # else:
                #     # print('fail ', my_result)
                #     print('comp ', result.decode())
        except Exception as error:
            logging.error(f'Error while sending: {error}')

    def disconnect(self):
        if self.connected:
            self.ts.close()
            self.connected = False
            dispatcher.send(signal="Status", status="Disconnected")

    def establish_telnet(self, ip_address, port):
        """Creates the telnet instance"""
        telnet_session = telnetlib.Telnet(ip_address, port, 5)
        telnet_session.set_option_negotiation_callback(self.call_back)
        return telnet_session

    def call_back(self, sock, cmd, opt):
        """ Turns on server side echoing"""
        if opt == telnetlib.ECHO and cmd in (telnetlib.WILL, telnetlib.WONT):
            sock.sendall(telnetlib.IAC + telnetlib.DO + telnetlib.ECHO)


def status(status):
    print(status)
    if status == "Connected":
        dispatcher.send('Telnet Command', command={'bcpu': 'BCPU1',
                                                   'ip_address': '192.168.7.66',
                                                   'mac_address': '20:16:B9:2A:64:EE',
                                                   'enable': False
                                                   })


def main():
    dispatcher.connect(status,
                       signal="Status",
                       sender=dispatcher.Any)
    my_control = Control('192.168.7.99')
    my_control.connect()
    time.sleep(5)
    my_control.disconnect()


if __name__ == '__main__':
    main()
