from threading import Thread
from pydispatch import dispatcher


class Commandjobs(Thread):

    def __init__(self, parent, queue):
        Thread.__init__(self)
        self.queue = queue
        self.parent = parent

    def run(self):
        while True:
            # gets the job from the queue
            job = self.queue.get()
            # print('Got job: ', job)
            self.send_command(job)
            # print('waiting for job done')

            # send a signal to the queue that the job is done
            self.queue.task_done()
            # print('job done')

    def send_command(self, job):
        dispatcher.send('Telnet Command', command=job, sender='CommandJobs Thread')
